/**
 * @param canvas canvas to manipulate
 * @param {Function} draw user function that does main drawing
 * @param dragBounds boundaries in which drag can be done.
 * @param {Number} dragBounds.leftTop.x x coordinate. 0 by default.
 * @param {Number} dragBounds.leftTop.y y coordinate. 0 by default.
 * @param {Number} dragBounds.rightBottom.x x coordinate. canvas.width by default.
 * @param {Number} dragBounds.rightBottom.y y coordinate. canvas.height by default.
 * @constructor
 */
var CanvasManipulation = function (canvas, draw, dragBounds) {
  this._draw = draw
  this._canvas = canvas
  this._initDragBounds(dragBounds)
  this._dragStartPoint = null
}

CanvasManipulation.prototype.init = function () {
  this._initCanvasContext()
}

/**
 * Sets canvas size by its bounds.
 * <code>canvas.width = canvas.offsetWidth; canvas.height = canvas.offsetHeight</code>
 * If canvas has inner elements such as zoom buttons then
 * they positioning and sizing better be done in this method. This method
 * useful for the mobile applications. If monitor changes orientation then you
 * can call this method and canvas will adapt new size accordingly.
 */
CanvasManipulation.prototype.layout = function () {
  this._calculateCanvasSize()
}

CanvasManipulation.prototype._calculateCanvasSize = function () {
  this._canvas.width = this._canvas.offsetWidth
  this._canvas.height = this._canvas.offsetHeight
}

CanvasManipulation.prototype._initCanvasContext = function () {
  this._ctx = this._canvas.getContext('2d')
  TrackTransform(this._ctx)
}

CanvasManipulation.prototype._initDragBounds = function (dragBounds) {
  dragBounds = dragBounds || {}
  this._dragBounds = {}
  if (!dragBounds['leftTop']) {
    this._dragBounds.leftTop = {x: 0, y: 0}
  } else {
    this._dragBounds.leftTop = dragBounds['leftTop']
  }
  if (!dragBounds['rightBottom']) {
    this._dragBounds.rightBottom = {x: this._canvas.width, y: this._canvas.height}
  } else {
    this._dragBounds.rightBottom = dragBounds['rightBottom']
  }
  //extending bounds so rotating of canvas will fit in
//  this._dragBounds.leftTop.x = this._dragBounds.leftTop.x - this._dragBounds.rightBottom.x
//  this._dragBounds.leftTop.y = this._dragBounds.leftTop.y - this._dragBounds.rightBottom.y

//  this._dragBounds.rightBottom.x = 2 * this._dragBounds.rightBottom.x
//  this._dragBounds.rightBottom.y = 2 * this._dragBounds.rightBottom.y
}

/*
 CanvasManipulation.prototype._inDragBounds = function (point) {
 var leftTop = this._dragBounds.leftTop
 var rightBottom = this._dragBounds.rightBottom
 return (point.x >= leftTop.x && point.y >= leftTop.y) && (point.x <= rightBottom.x && point.y <= rightBottom.y)
 }
 */

CanvasManipulation.prototype._canDrag = function (value, min, max, dragValue) {
  return (value >= min || (value < min && dragValue < 0)) && (value <= max || (value > max && dragValue > 0))
}

CanvasManipulation.prototype._dragDistance = function (startPoint, endPoint) {
  var leftTop = this._dragBounds.leftTop
  var rightBottom = this._dragBounds.rightBottom
  var pt0 = this._ctx.transformedPoint(0, 0)
  var distanceX = endPoint.x - startPoint.x
  if (!this._canDrag(pt0.x, leftTop.x, rightBottom.x, distanceX)) {
    distanceX = 0
  }
  var distanceY = endPoint.y - startPoint.y
  if (!this._canDrag(pt0.y, leftTop.y, rightBottom.y, distanceY)) {
    distanceY = 0
  }
  return {x: distanceX, y: distanceY}
}

/**
 * @param onPoint drag start point
 * @param {Number} onPoint.x x coordinate
 * @param {Number} onPoint.y y coordinate
 */
CanvasManipulation.prototype.dragStart = function (onPoint) {
  var pt = this._ctx.transformedPoint(onPoint.x, onPoint.y)
  //if (this._inDragBounds(pt)) {
  this._dragStartPoint = pt
  //}
}

/**
 * @param onPoint point where to drag. Drag won't work if dragStart method hasn't been called.
 * @param {Number} onPoint.x x coordinate
 * @param {Number} onPoint.y y coordinate
 */
CanvasManipulation.prototype.drag = function (onPoint) {
  if (this._dragStartPoint) {
    var pt = this._ctx.transformedPoint(onPoint.x, onPoint.y)
    var distance = this._dragDistance(this._dragStartPoint, pt)
    this._ctx.translate(distance.x, distance.y)
    this._draw()
  }
}

CanvasManipulation.prototype.dragEnd = function () {
  this._dragStartPoint = null
}

CanvasManipulation.prototype.zoomFactor = 1.1
CanvasManipulation.prototype.zoomMax = 10
CanvasManipulation.prototype.zoomMin = -10
CanvasManipulation.prototype.zoomCurrent = 1

/**
 * @param onPoint zoom center
 * @param {Number} onPoint.x x coordinate
 * @param {Number} onPoint.y y coordinate
 * @param {Number} value zoom value. Max is 10. Min is -10.
 * Zoom factor formula is <code>Math.pow(1.1, value)</code>
 */
CanvasManipulation.prototype.zoom = function (onPoint, value) {
  var pt = this._ctx.transformedPoint(onPoint.x, onPoint.y)
  this._ctx.translate(pt.x, pt.y)
  if (this.zoomCurrent + value > this.zoomMax) {
    value = this.zoomMax - this.zoomCurrent
  }
  if (this.zoomCurrent + value < this.zoomMin) {
    value = this.zoomMin - this.zoomCurrent
  }
  this.zoomCurrent = this.zoomCurrent + value
  var factor = Math.pow(this.zoomFactor, value)
  this._ctx.scale(factor, factor)
  //this.updateBoundsScale(factor)
  this._ctx.translate(-pt.x, -pt.y)
  this._draw()
}

/**
 * @param center rotation center
 * @param {Number} center.x x coordinate
 * @param {Number} center.y y coordinate
 * @param {Number} radians rotation angle in radians
 */
CanvasManipulation.prototype.rotate = function (center, radians) {
  var pt = this._ctx.transformedPoint(center.x, center.y)
  this._ctx.translate(pt.x, pt.y)
  this._ctx.rotate(radians)
  //this.updateBoundsRotate(angle)
  this._ctx.translate(-pt.x, -pt.y)
  this._draw()
}

CanvasManipulation.prototype.getCanvas = function () {
  return this._canvas
}

/*
 TODO more careful implementation of dragBounds
 CanvasManipulation.prototype.updateBoundsScale = function (scale) {
 var matrix = new SvgMatrixSub()
 this.updateBounds(matrix.scaleNonUniform(scale, scale).inverse())
 }

 CanvasManipulation.prototype.updateBoundsRotate = function (angle) {
 var matrix = new SvgMatrixSub()
 this.updateBounds(matrix.rotate(angle))
 }

 CanvasManipulation.prototype.updateBounds = function (matrix) {
 this._dragBounds.leftTop = this._ctx.matrixTransformPoint(this._dragBounds.leftTop.x, this._dragBounds.leftTop.y, matrix)
 this._dragBounds.rightBottom = this._ctx.matrixTransformPoint(this._dragBounds.rightBottom.x, this._dragBounds.rightBottom.y, matrix)
 }
 */
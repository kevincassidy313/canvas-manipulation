/**
 * Mixin which enables canvas context to save applied transformations.
 * @param canvasContext canvas.getContext('2d')
 */
var TrackTransform = function (canvasContext) {
  var svg = new SvgTransformSub()
  var transformMatrix = svg.createSVGMatrix()

  canvasContext.getTransformMatrix = function () {
    return transformMatrix
  }


  var savedTransforms = []
  var save = canvasContext.save
  canvasContext.save = function () {
    savedTransforms.push(transformMatrix.translate(0, 0))
    return save.call(canvasContext)
  }
  var restore = canvasContext.restore
  canvasContext.restore = function () {
    transformMatrix = savedTransforms.pop()
    return restore.call(canvasContext)
  }


  var scale = canvasContext.scale
  canvasContext.scale = function (sx, sy) {
    transformMatrix = transformMatrix.scaleNonUniform(sx, sy)
    return scale.call(canvasContext, sx, sy)
  }

  var rotate = canvasContext.rotate
  canvasContext.rotate = function (radians) {
    transformMatrix = transformMatrix.rotate(radians)
    return rotate.call(canvasContext, radians)
  }

  var translate = canvasContext.translate
  canvasContext.translate = function (dx, dy) {
    transformMatrix = transformMatrix.translate(dx, dy)
    return translate.call(canvasContext, dx, dy)
  }

  var transform = canvasContext.transform
  canvasContext.transform = function (a, b, c, d, e, f) {
    var matrix = svg.createSVGMatrix()
    matrix.setTransformFromArray(a, b, c, d, e, f)
    transformMatrix = transformMatrix.multiply(matrix)
    return transform.call(canvasContext, a, b, c, d, e, f)
  }

  var setTransform = canvasContext.setTransform
  canvasContext.setTransform = function (a, b, c, d, e, f) {
    transformMatrix.setTransformFromArray(a, b, c, d, e, f)
    return setTransform.call(canvasContext, a, b, c, d, e, f)
  }

  canvasContext.setTransformMatrix = function (matrix) {
    transformMatrix = matrix
    setTransform.apply(canvasContext, matrix.getTransformAsArray())
  }


  var pt = svg.createSVGPoint()
  canvasContext.transformedPoint = function (x, y) {
    pt.x = x
    pt.y = y
    return pt.matrixTransform(transformMatrix.inverse())
  }


  canvasContext.clearTransformedRect = function (x, y, w, h) {
    var p1 = canvasContext.transformedPoint(x, y)
    var p2 = canvasContext.transformedPoint(x + w, y + h)
    canvasContext.clearRect(p1.x, p1.y, p2.x - p1.x, p2.y - p1.y)
  }

  canvasContext.clearCanvas = function () {
    canvasContext.save()
    // Use the identity matrix while clearing the canvas
    canvasContext.setTransform(1, 0, 0, 1, 0, 0)
    canvasContext.clearRect(0, 0, canvasContext.canvas.width, canvasContext.canvas.height)
    canvasContext.restore()
  }

}
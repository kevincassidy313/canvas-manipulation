/**
 * Created: vogdb Date: 9/8/13 Time: 1:44 PM
 */
function Matrix(values) {
  if (!Array.isArray(values)) {
    throw new Error('Array must be passed')
  }
  if (values.length !== Matrix.rowLen) {
    throw new Error('Matrix must have 3 rows')
  }
  this._values = []
  for (var rowIndex = 0; rowIndex < Matrix.rowLen; rowIndex++) {
    var row = values[rowIndex]
    if (values.length !== Matrix.colLen) {
      throw new Error('Matrix must have 3 cols on row: ' + rowIndex)
    }
    this._values[rowIndex] = []
    for (var colIndex = 0; colIndex < Matrix.colLen; colIndex++) {
      this._values[rowIndex][colIndex] = values[rowIndex][colIndex];
    }
  }
}

Matrix.rowLen = 3;
Matrix.colLen = 3;

Matrix.prototype.multiply = function (matrix) {
  if (Array.isArray(matrix)) {
    matrix = new Matrix(matrix);
  }

  if (!matrix instanceof Matrix) {
    throw new Error('Argument passed is not a valid Matrix object');
  }

  var product = new Matrix([
    [1, 0, 0],
    [0, 1, 0],
    [0, 0, 1]
  ])

  var leftM = this._values
  var rightM = matrix._values
  for (var rowIndex = 0; rowIndex < Matrix.rowLen; rowIndex++) {
    for (var colIndex = 0; colIndex < Matrix.colLen; colIndex++) {
      var square = 0
      for (var i = 0; i < Matrix.colLen; i++) {
        square += leftM[rowIndex][i] * rightM[i][colIndex]
      }
      product._values[rowIndex][colIndex] = square
    }
  }
  delete this._values
  this._values = product._values
  product._values = null
  return this
}

Matrix.prototype.clone = function () {
  return new Matrix(this._values)
}

Matrix.prototype.inverse = function () {
  // Declare variables
  var ratio
  var a
  var rowLen = Matrix.rowLen
  var values = this._values

  // Put an identity matrix to the right of matrix
  for (var i = 0; i < rowLen; i++) {
    for (var j = rowLen; j < 2 * Matrix.colLen; j++) {
      if (i === (j - rowLen)) {
        values[i][j] = 1
      }
      else {
        values[i][j] = 0
      }
    }
  }

  for (var i = 0; i < rowLen; i++) {
    for (var j = 0; j < rowLen; j++) {
      if (i !== j) {
        ratio = values[j][i] / values[i][i]
        for (var k = 0; k < 2 * rowLen; k++) {
          values[j][k] -= ratio * values[i][k]
        }
      }
    }
  }

  for (var i = 0; i < rowLen; i++) {
    a = values[i][i]
    for (var j = 0; j < 2 * rowLen; j++) {
      values[i][j] /= a
    }
  }

  // Remove the left-hand identity matrix
  for (var i = 0; i < rowLen; i++) {
    values[i].splice(0, rowLen)
  }

  return this
}